import React, { Component } from 'react';
import { SafeAreaView } from 'react-native';
import HomeScreen from './app/screens/homeScreen/index';

export default class App extends Component {
  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <HomeScreen />
      </SafeAreaView>
    );
  }
}
