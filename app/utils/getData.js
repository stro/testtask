import { MAIN_ROUTE, HEADERS } from '../const/general';

export const getAllData = async () => {
  const response = await fetch(MAIN_ROUTE + 'games', {
    method: 'GET',
    headers: HEADERS,
  });
  const data = await response.json();

  return data.data.map((item) => {
    return {
      name: item.names.twitch,
      imageUrl: item.assets['cover-medium'].uri,
    };
  });
};
