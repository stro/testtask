import React, { Component } from 'react';
import { Text, View, ViewPropTypes, T } from 'react-native';
import PropsTypes from 'prop-types';
import { NO_DATA } from '../../const/lang';
import styles from './style';

class EmptyListComponsnt extends Component {

  static propTypes = {
    text: PropsTypes.string,
    textStyle: Text.propTypes.style,
    style: ViewPropTypes.style,
  }

  static defaultProps = {
    text: NO_DATA,
  }

  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <Text style={[styles.textStyle, this.props.textStyle]}>{this.props.text}</Text>
      </View>
    )
  }
}

export default EmptyListComponsnt;
