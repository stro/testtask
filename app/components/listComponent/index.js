import React, { Component } from 'react';
import {FlatList, Text, View, ViewPropTypes} from 'react-native';
import PropsTypes from 'prop-types';
import EmptyComponent from '../emptyComponent/index';
import styles from './style';

class List extends Component {

  constructor() {
    super();

    this.state = {
      width: 0,
      height: 0,
    };

    this.calculateLayout = this.calculateLayout.bind(this);
  }

  static propTypes = {
    items: PropsTypes.oneOfType([
      PropsTypes.array,
      PropsTypes.object,
      PropsTypes.arrayOf(PropsTypes.object),
    ]),
    renderItem: PropsTypes.func.isRequired,
    separator: PropsTypes.func,
    keyExtractor: PropsTypes.func,
    contentStyle: ViewPropTypes.style,
    style: ViewPropTypes.style,
    footer: PropsTypes.func,
    header: PropsTypes.func,
    emptyTextStyle: Text.propTypes.style,
    emptyText: PropsTypes.string,
    empty: PropsTypes.func,
  }

  static defaultProps = {
    items: [],
    keyExtractor: (item, index) => item + index,
  }

  static renderLine() {
    return (
      <View style={styles.line} />
    )
  }

  calculateLayout(event) {
    const { width, height } = event.nativeEvent.layout;
    this.setState({ width, height });
  }

  render() {
    return (
      <FlatList
        onLayout={this.calculateLayout}
        data={this.props.items}
        renderItem={this.props.renderItem}
        ItemSeparatorComponent={this.props.separator ? this.props.separator : List.renderLine}
        ListHeaderComponent={this.props.header}
        ListFooterComponent={this.props.footer}
        keyExtractor={this.props.keyExtractor}
        contentContainerStyle={this.props.contentStyle}
        style={[styles.container, this.props.style]}
        ListEmptyComponent={
          this.props.empty
          ? this.props.empty
          : (
              <EmptyComponent
                text={this.props.emptyText}
                textStyle={this.props.emptyTextStyle}
                style={{ width: this.state.width, height: this.state.height }}
              />
          )
        }
      />
    )
  }
}

export default List;
