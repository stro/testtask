import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';
import PropsTypes from 'prop-types';
import { NAME_IS } from '../../const/lang';
import styles from './style';

class GamePreview extends Component {

  static propTypes = {
    name: PropsTypes.string,
    imageUrl: PropsTypes.string,
  }

  render() {
    return (
      <View style={styles.container}>
        <Image
          resizeMode={'cover'}
          source={{ uri: this.props.imageUrl }}
          style={styles.imgStyle}
        />
        <View style={styles.contaienrForText}>
          <Text style={styles.textName}>{NAME_IS}</Text>
          <Text style={styles.nameOfGame}>{this.props.name}</Text>
        </View>
      </View>
    )
  }
}

export default GamePreview;
