import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingVertical: 15,
    alignItems: 'center',
  },
  imgStyle: {
    width: 50,
    height: 100,
  },
  nameOfGame: {
    fontSize: 18,
    color: '#000',
    marginTop: 5,
  },
  contaienrForText: {
    flex: 1,
    paddingHorizontal: 20,
  },
  textName: {
    fontSize: 16,
    color: 'gray',
  },
});
