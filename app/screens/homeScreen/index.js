import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { getAllData } from '../../utils/getData';
import List from '../../components/listComponent/index';
import GamePreview from '../../components/gamePreview/index';
import { SOMETHING_WENT_WRONG } from '../../const/lang';
import styles from './style';

class HomeScreen extends Component {

  constructor() {
    super();

    this.state = {
      items: [],
      error: '',
    };
  }

  async componentDidMount() {
    try {
      const shortData = await getAllData();

      this.setState({
        items: shortData,
        error: '',
      });
    } catch (error) {
      this.setState({ error: SOMETHING_WENT_WRONG });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <List
          items={this.state.items}
          renderItem={({ item }) => <GamePreview name={item.name} imageUrl={item.imageUrl}/>}
          emptyText={this.state.error ? this.state.error : undefined}
        />
      </View>
    )
  }
}

export default HomeScreen;
